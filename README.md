# Aplikacja fastapi

Zwraca IPv4 klienta

## Budowanie obrazu

```sh
docker-compose build
```

## Uruchomienie kontenera

```sh
docker-compose up
```

## Endpoint do pobierania IPv4 klienta

`localhost:8000`

## Dokumentacja aplikacji

Jeżeli zmienna środowiskowa `$API_PREFIX` jest zmieniona, to dokumentacji należy szukać pod linkiem:

`localhost:8000/$API_PREFIX/docs`

Openapi:

[localhost:8000/docs](localhost:8000/docs)

Redoc:

[localhost:8000/redoc](localhost:8000/redoc)

## Informacje po uruchomieniu obrazu wyświetlone w terminalu

```
$ docker-compose up
Starting app_1 ... done
Attaching to app_1
app_1  | INFO:     Will watch for changes in these directories: ['/service']
app_1  | INFO:     Uvicorn running on http://0.0.0.0:8000 (Press CTRL+C to quit)
app_1  | INFO:     Started reloader process [7] using statreload
app_1  | INFO:     Started server process [9]
app_1  | INFO:     Waiting for application startup.
app_1  | Autor:
app_1  | {'name': 'Przemysław Markiewicz', 'url': 'https://gitlab.com/Kaczor3213', 'email': 'kacz.pacz@gmail.com'}
app_1  | INFO:     Application startup complete.
```

## Warstwy obrazu

```
$ docker history fastapi_application
IMAGE          CREATED              CREATED BY                                      SIZE      COMMENT
e4072efd60f7   About a minute ago   ENTRYPOINT ["sh" "./entrypoint.sh"]             0B        buildkit.dockerfile.v0
<missing>      About a minute ago   RUN /bin/sh -c chmod +x $APP_ROOT/entrypoint…   87B       buildkit.dockerfile.v0
<missing>      About a minute ago   COPY . /service/ # buildkit                     5.54kB    buildkit.dockerfile.v0
<missing>      21 minutes ago       RUN /bin/sh -c apk upgrade --available &&   …   56.9MB    buildkit.dockerfile.v0
<missing>      21 minutes ago       ADD requirements.txt /service/ # buildkit       59B       buildkit.dockerfile.v0
<missing>      2 weeks ago          WORKDIR /service/                               0B        buildkit.dockerfile.v0
<missing>      2 weeks ago          RUN /bin/sh -c mkdir $APP_ROOT # buildkit       0B        buildkit.dockerfile.v0
<missing>      2 weeks ago          ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECO…   0B        buildkit.dockerfile.v0
<missing>      2 weeks ago          LABEL author=Przemysław Markiewicz, kacz.pac…   0B        buildkit.dockerfile.v0
<missing>      2 weeks ago          /bin/sh -c #(nop)  CMD ["python3"]              0B
<missing>      2 weeks ago          /bin/sh -c set -eux;   wget -O get-pip.py "$…   10.2MB
<missing>      2 weeks ago          /bin/sh -c #(nop)  ENV PYTHON_GET_PIP_SHA256…   0B
<missing>      2 weeks ago          /bin/sh -c #(nop)  ENV PYTHON_GET_PIP_URL=ht…   0B
<missing>      2 weeks ago          /bin/sh -c #(nop)  ENV PYTHON_SETUPTOOLS_VER…   0B
<missing>      2 weeks ago          /bin/sh -c #(nop)  ENV PYTHON_PIP_VERSION=22…   0B
<missing>      2 weeks ago          /bin/sh -c set -eux;  for src in idle3 pydoc…   32B
<missing>      2 weeks ago          /bin/sh -c set -eux;   apk add --no-cache --…   30MB
<missing>      4 weeks ago          /bin/sh -c #(nop)  ENV PYTHON_VERSION=3.9.12    0B
<missing>      4 weeks ago          /bin/sh -c #(nop)  ENV GPG_KEY=E3FF2839C048B…   0B
<missing>      4 weeks ago          /bin/sh -c set -eux;  apk add --no-cache   c…   1.8MB
<missing>      4 weeks ago          /bin/sh -c #(nop)  ENV LANG=C.UTF-8             0B
<missing>      4 weeks ago          /bin/sh -c #(nop)  ENV PATH=/usr/local/bin:/…   0B
<missing>      4 weeks ago          /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing>      4 weeks ago          /bin/sh -c #(nop) ADD file:5d673d25da3a14ce1…   5.57MB
```

