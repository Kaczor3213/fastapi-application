from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.requests import Request
from src import config


async def on_app_start():
    print(f"Autor:\n{config.CONTACT}")

def get_application():
    app = FastAPI(
        debug=True,
        title=config.MICROSERVICE,
        description=config.DESCRIPTION,
        version=config.VERSION,
        docs_url=config.DOCS_URL,
        redoc_url=config.REDOC_URL,
        contact=config.CONTACT,
        on_startup=[on_app_start],
        license_info={
            "name": "MIT",
            "url": "https://localhost"
        }
    )
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return app


app = get_application()

@app.get("/")
async def get_ip_timestamp(request: Request):
    return {'client_ip': request.client.host}