import os
from pathlib import Path
from pydantic import SecretStr, BaseSettings


class Config(BaseSettings):
    BASE_DIR = Path(__file__).resolve().parent.parent
    APP_DIR = Path(__file__).resolve().parent
    MICROSERVICE: str = "Fastapi"
    VERSION: str = "1.0.0"
    DESCRIPTION: str = "Aplikacja zwracająca adres IpV4 klienta."
    CONTACT: dict = {
        "name": "Przemysław Markiewicz",
        "url": "https://gitlab.com/Kaczor3213",
        "email": "kacz.pacz@gmail.com",
    }
    API_PREFIX = os.getenv("API_PREFIX", "")
    DOCS_URL = API_PREFIX + "/docs"
    REDOC_URL = API_PREFIX + "/redoc"
    TEST_ENV: bool = os.getenv("TEST_ENV", False)
    SECRET_KEY: SecretStr = os.getenv("SECRET_KEY", "dupa")
