from datetime import date
from typing import Optional

from absbbfastapilib.redis.utils import AbsBbFastapiRedisOrderBy
from fastapi import APIRouter, BackgroundTasks, Body, Depends
from starlette.status import HTTP_200_OK

from src.controllers import LogController, get_log_controller
from src.schemas import Log, LogList, LogRequest, LogSeverity, LogTask

router = APIRouter(
    prefix="/log",
    tags=[
        "logger",
    ],
)


@router.post(
    "/",
    name="log:create",
    response_model=LogTask,
    status_code=HTTP_200_OK,
)
async def exec_log(
    background_tasks: BackgroundTasks,
    log: LogRequest = Body(...),
    log_ctrl: LogController = Depends(get_log_controller),
) -> LogTask:
    background_tasks.add_task(log_ctrl.create_log, Log(**log.dict()))
    return LogTask(status="logged")


@router.get("/", name="log:get", response_model=LogList, status_code=HTTP_200_OK)
async def get_log(
    date_from: Optional[date] = None,
    date_to: Optional[date] = None,
    page_no: Optional[int] = 0,
    page_size: Optional[int] = 10,
    level: Optional[LogSeverity] = None,
    filter: Optional[str] = None,
    sort_by: Optional[str] = "created_at",
    order: Optional[AbsBbFastapiRedisOrderBy] = AbsBbFastapiRedisOrderBy.DESC,
    log_ctrl: LogController = Depends(get_log_controller),
) -> LogList:
    return LogList(
        log_list=await log_ctrl.get_logs(
            date_from=date_from,
            date_to=date_to,
            page_no=page_no,
            page_size=page_size,
            level_filter=level,
            description_filter=filter,
            sort_by=sort_by,
            order=order,
        )
    )
