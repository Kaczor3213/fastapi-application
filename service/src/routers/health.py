import traceback

from absbbfastapilib.redis.resolver import AbsBbFastapiRedisResolver
from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from starlette.status import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR

from src import config

router = APIRouter(tags=["healthcheck"])


@router.get("/alive", name="health:alive", status_code=HTTP_200_OK)
async def alive_check():
    return "healthy"


@router.get("/ready", name="health:ready", status_code=HTTP_200_OK)
async def ready_check():
    resolver = AbsBbFastapiRedisResolver()
    redis = resolver.get_redis_connection(config.REDIS_DB, socket_connect_timeout=1)
    try:
        await redis.ping()
    except (ConnectionError, TimeoutError):
        raise HTTPException(
            status_code=HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Baza danych nie jest gotowa:\n{traceback.format_exc()}",
        )
    return "ready"
